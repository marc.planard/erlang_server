-module(route).
-include_lib("eunit/include/eunit.hrl").
-export([route/3, send_response/4]).

route([], Sock, _Req) ->
    send_response(Sock, 200, "text/plain", "Hello world\n");
route(["time"], Sock, _Req) ->
    {_Date,{H,M,S}} = calendar:local_time(),
    send_response(Sock, 200, "text/plain", 
		  io_lib:format("Time: ~B:~B:~B~n", [H,M,S]));
route(["pub"|Path], Sock, _Req) ->
    Ret = file:read_file(string:join(["pub"]++Path, "/")),
    case Ret of
	{ok, Data} -> send_response(Sock, 200, get_type(Path), Data);
	{error,enoent} -> 
	    send_response(Sock, 404, "text/plain", "404 Not Found\n");
	{error,eaccess} ->
	    send_response(Sock, 403, "text/plain", "403 Forbidden\n");
	{error,eisdir} ->
	    send_response(Sock, 403, "text/plain", 
			  "Directory listing forbidden\n");
	{E,V} ->
	    send_response(Sock, 500, "text/plain", 
			  io_lib:format("??? ~p ~p~n", [E,V]))
    end;
route(["crash"], _Sock, _Req) ->
    io:fwrite("About to crash!~n", []),
    toto = tata;
route(Path, Sock, _Req) ->
    send_response(Sock, 404, "text/plain", 
		  io_lib:format("Unknown path: ~p~n", [Path])).

send_response(Sock, Status, Type, Content) ->
    gen_tcp:send(Sock, headers(Status, Type, 
			       byte_size(iolist_to_binary(Content)))),
    gen_tcp:send(Sock, Content).

headers(Status, Type, Size) ->
    io_lib:format("HTTP/1.1 ~B ~s~n"
		  "Server: MyErlangServer~n"
		  "Content-Length: ~B~n"
		  "Content-Type: ~s~n~n"
		 , [Status, status(Status), Size, Type]).

status(200) -> "OK";
status(403) -> "Forbidden";
status(404) -> "Not Found";
status(500) -> "Internal Server Error";
status(_) -> "Unknown Status :(".
	       
get_type(Path) ->
    Ext = lists:last(string:tokens(lists:last(Path), ".")),
    mime_type(Ext).

mime_type("html") -> "text/html";
mime_type("png") ->  "image/png";
mime_type("jpg") -> "image/jpeg";
mime_type("jpeg") -> "image/jpeg";
mime_type("txt") -> "text/plain";
mime_type(_) -> "application/octet-stream".
