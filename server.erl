-module(server).
-export([run/0, run/1, server/1, server/2, server_loop/3]).
-import(servworker, [net_send/2]).

run() -> run(1337).
run(Port) -> spawn(server, server, [Port]).

server(Port) -> server(Port, self()).
server(Port, Pid) ->
    {ok, LSock} = gen_tcp:listen(Port, [binary, {packet, 0}, 
                                        {active, false}, {reuseaddr, true}]),
    Looper = spawn(server, server_loop, [0, Pid, LSock]),
    event_loop(),
    exit(Looper, kill),
    gen_tcp:close(LSock).

server_loop(stop, _, _) -> ok;
server_loop(N, Parent, LSock) ->
    case gen_tcp:accept(LSock) of
	{ ok, Sock } ->
	    Name = lists:flatten(io_lib:format("~s~B",["Anon", N])),
	    NextN = N+1,
	    Parent ! { sendAll, io_lib:format("*** ~s connected.~n", [Name]) },
	    spawn(servworker, do_recv, [continue, Parent, Sock, Name]),
	    Parent ! { new, Sock, {Sock, Name} };
	{ Ret, Sock } ->
	    io:fwrite("Erf? [~p] :: ~p~n", [Ret, Sock]),
	    NextN = stop
    end,
    server_loop(NextN, Parent, LSock).
    
event_loop() -> event_loop(#{}, []).
event_loop(ClientSocks, SrvLst) ->
    receive
	{ new, Sock, Body } -> new_client(Sock, Body, ClientSocks, SrvLst);
	{ close, Sock } -> close_client(Sock, ClientSocks, SrvLst);
	{ changeNick, Sock, Nick } -> change_nick(Sock,Nick,ClientSocks,SrvLst);
	{ listUsers, Sock } -> list_users(Sock, ClientSocks, SrvLst);
	{ sendAll, Msg } -> send_all(Msg, ClientSocks, SrvLst, SrvLst);
	{ link, Srv } -> add_link(Srv, ClientSocks, SrvLst);
	{ srvAll, Msg } -> send_all(Msg, ClientSocks, SrvLst, []);
	info -> display_info(ClientSocks, SrvLst);
	stop -> shutdown()
    end.

shutdown() -> 	    
    io:fwrite("Shutting down...~n").

add_link(Srv, ClientSocks, SrvLst) ->
    case lists:member(Srv, SrvLst) of
	true  -> event_loop(ClientSocks, SrvLst);
	false -> io:fwrite("Adding node: ~p~n", [Srv]),
		 {Reg,_Host} = Srv,
		 Msg = { link, { Reg, node() }},
		 io:fwrite("Propagate to: ~p ! ~p~n", [Srv, Msg]),
		 Srv ! Msg ,
		 [ Pid ! Srv || Pid <- SrvLst ],
		 event_loop(ClientSocks, SrvLst ++ [Srv])
    end.

display_info(ClientSocks, SrvLst) ->
    Lstr = [ io_lib:format("    ~p : ~s~n", [ Sock, Name ]) || 
	       { Sock, Name} <- maps:values(ClientSocks) ],
    io:fwrite("Clients:~n~s", [string:join(Lstr, "")]),
    io:fwrite("Links: ~p~n", [SrvLst]),
    event_loop(ClientSocks, SrvLst).
	     
net_send(Sock, Msg, OnErrorFct) ->
    Ret = net_send(Sock, Msg),
    case Ret of
	ok -> ok;
	{error,_} -> OnErrorFct(Sock)
    end.
	     
send_all(Msg, ClientSocks, SrvLst, SrvPropagate) ->
    [ net_send(Sock, Msg, fun(S) -> self() ! { close, S } end) || 
	Sock <- maps:keys(ClientSocks) ],
    [ Pid ! { srvAll, Msg } || Pid <- SrvPropagate ],
    event_loop(ClientSocks, SrvLst).

list_users(Sock, ClientSocks, SrvLst) ->
    Users = string:join([ Name || {_,Name} <- maps:values(ClientSocks)], ", "),
    net_send(Sock, io_lib:format("*** Users: ~s~n", [Users])),
    event_loop(ClientSocks, SrvLst).

change_nick(Sock, Nick, ClientSocks, SrvLst) ->
    #{Sock := {Sock,OldNick}} = ClientSocks,
    self() ! { sendAll, io_lib:format("*** ~s is now ~s~n", [OldNick, Nick]) },
    event_loop(ClientSocks#{ Sock := {Sock,Nick}}, SrvLst).

new_client(Sock, Body, ClientSocks, SrvLst) ->
    NewClientSocks = ClientSocks#{ Sock => Body},
    io:fwrite("New client: ~p :: ~p~n", [Sock, Body]),
    event_loop(NewClientSocks, SrvLst).

close_client(Sock, ClientSocks, SrvLst) ->
    #{Sock := {Sock,Name}} = ClientSocks,
    NewClientSocks = maps:filter(fun(K,_V) -> K/=Sock end, ClientSocks),
    io:fwrite("Client disconnected: ~s ~p~n", [Name, Sock]),
    self() ! { sendAll, io_lib:format("*** ~s left.~n", [Name]) },
    if
	NewClientSocks /= #{} ->
	    event_loop(NewClientSocks, SrvLst);
	true -> 
	    io:fwrite("No clients, auto-shutdown...~n")
    end.
