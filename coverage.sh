#!/bin/sh

rm -f servworker.COVER.out

erl << EOF
cover:start().
cover:compile_module(servworker).
servworker:test().
cover:analyse_to_file(servworker).
EOF

