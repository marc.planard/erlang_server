-module(httpd).
-include_lib("eunit/include/eunit.hrl").
-export([run/0, run/1, server/1, server_loop/3, do_recv/2]).
-import(route, [route/3, send_response/4]).

run() -> run(8080).
run(Port) -> spawn(?MODULE, server, [Port]).

server(Port) ->
    {ok, LSock} = gen_tcp:listen(Port, [binary, {packet, http}, 
                                        {active, false}, {reuseaddr, true}]),
    io:fwrite("Listening on port ~B~n", [Port]),
    Looper = spawn(?MODULE, server_loop, [continue, self(), LSock]),
    event_loop(),
    exit(Looper, kill),
    gen_tcp:close(LSock).

server_loop(stop, _, _) -> ok;
server_loop(continue, Parent, LSock) ->
    case gen_tcp:accept(LSock) of
	{ ok, Sock } ->
	    spawn(?MODULE, do_recv, [ Sock, #{} ]),
	    Next = continue;
	{ Ret, Sock } ->
	    io:fwrite("Erf? [~p] :: ~p~n", [Ret, Sock]),
	    Next = stop
    end,
    server_loop(Next, Parent, LSock).
    
event_loop() ->
    receive
	stop -> shutdown()
    end.

shutdown() -> 	    
    io:fwrite("Shutting down...~n").

do_recv(Sock, stop) -> 		    
    gen_tcp:close(Sock);
do_recv(Sock, Req) ->
    case gen_tcp:recv(Sock, 0, 10000) of
	{ok, http_eoh} ->
	    process_req(Sock, Req),
	    NewReq = stop;
        {ok, B} ->
	    NewReq = parse_buffer(Req, B);
	{error, Cause} ->
	    io:fwrite("~p :: Socket closed: ~p~n", [Sock, Cause]),
	    NewReq = stop
    end,
    do_recv(Sock, NewReq).

parse_buffer(Req, B) ->
    io:fwrite(">>> ~p~n", [B]),
    case B of
	{http_request,Method,{abs_path,Path},{1,1}} ->
	    Req#{ method => Method, abs_path => Path};
	{http_header,_,Key,_,Value} ->
	    Req#{ Key => Value }
    end.

process_req(Sock, Req) ->
    io:fwrite("!!! ~p~n", [Req]),
    Tail = string:tokens(maps:get(abs_path, Req), "/"),
    try route(Tail, Sock, Req) of
	_ -> ok
    catch
	Exception:Reason -> io:fwrite("Crashed: ~p~n", 
				      [{caught, Exception, Reason}]),
			    send_response(Sock, 500, "text/plain", 
					  "Internal Server Error\n")
    end.
